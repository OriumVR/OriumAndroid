﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ShowSceneHandleBack : MonoBehaviour {

    public Cardboard cardboard;

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape) || 
            (cardboard != null && cardboard.BackButtonPressed))
        {
            SceneManager.LoadSceneAsync("UIScene");
        }
	}
}
