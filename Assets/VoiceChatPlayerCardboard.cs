using System;
using UnityEngine;

// 'Spotlight' sound: http://answers.unity3d.com/questions/148339/is-directional-sound-possible.html
using System.Threading;

namespace VoiceChat
{
    [RequireComponent(typeof(CardboardAudioSource))]
    public class VoiceChatPlayerCardboard : MonoBehaviour, IVoiceChatPlayer
    {
        float lastTime = 0;
        double played = 0;
        double received = 0;
        int index = 0;
        float[] data;
        float playDelay = 0;
        bool shouldPlay = false;
        float lastRecvTime = 0;
        NSpeex.SpeexDecoder speexDec = new NSpeex.SpeexDecoder(NSpeex.BandMode.UltraWide);

        [SerializeField]
        int playbackDelay = 2;

        public float LastRecvTime
        {
            get { return lastRecvTime; }
        }

        void Start()
        {
            int size = VoiceChatSettings.Instance.Frequency * 10;

            GetComponent<CardboardAudioSource>().loop = true;
            GetComponent<CardboardAudioSource>().clip = AudioClip.Create("VoiceChat", size, 1, VoiceChatSettings.Instance.Frequency, false);
            data = new float[size];

            if (VoiceChatSettings.Instance.LocalDebug)
            {
                VoiceChatRecorder.Instance.NewSample += OnNewSample;
            }
        }

        void Update()
        {
            if (GetComponent<CardboardAudioSource>().isPlaying)
            {
                // Wrapped around
                if (lastTime > GetComponent<CardboardAudioSource>().time)
                {
                    played += GetComponent<CardboardAudioSource>().clip.length;
                }

                lastTime = GetComponent<CardboardAudioSource>().time;

                // Check if we've played to far
                if (played + GetComponent<CardboardAudioSource>().time >= received)
                {
                    Stop();
                    shouldPlay = false;
                }
            }
            else
            {
                if (shouldPlay)
                {
                    playDelay -= Time.deltaTime;

                    if (playDelay <= 0)
                    {
                        GetComponent<CardboardAudioSource>().Play();
                    }
                }
            }
        }

        void Stop()
        {
            GetComponent<CardboardAudioSource>().Stop();
            GetComponent<CardboardAudioSource>().time = 0;
            index = 0;
            played = 0;
            received = 0;
            lastTime = 0;
        }

        public void OnNewSample(VoiceChatPacket packet)
        {
            // Store last packet

            // Set last time we got something
            lastRecvTime = Time.time;

            // Decompress
            float[] sample = null;
            int length = VoiceChatUtils.Decompress(speexDec, packet, out sample);

            // Add more time to received
            received += VoiceChatSettings.Instance.SampleTime;

            // Push data to buffer
            Array.Copy(sample, 0, data, index, length);

            // Increase index
            index += length;

            // Handle wrap-around
            if (index >= GetComponent<CardboardAudioSource>().clip.samples)
            {
                index = 0;
            }

            // Set data
            GetComponent<CardboardAudioSource>().clip.SetData(data, 0);

            // If we're not playing
            if (!GetComponent<CardboardAudioSource>().isPlaying)
            {
                // Set that we should be playing
                shouldPlay = true;

                // And if we have no delay set, set it.
                if (playDelay <= 0)
                {
                    playDelay = (float)VoiceChatSettings.Instance.SampleTime * playbackDelay;
                }
            }

            VoiceChatFloatPool.Instance.Return(sample);
        }
    } 
}