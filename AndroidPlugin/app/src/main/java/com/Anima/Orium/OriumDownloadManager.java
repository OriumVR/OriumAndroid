package com.Anima.Orium;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import com.unity3d.player.UnityPlayer;


public class OriumDownloadManager extends BroadcastReceiver {

    public static final String SHARED_PREFS_NAME = OriumDownloadManager.class.getCanonicalName();
    public static final String ACTIVE_DOWNLOAD_IDS = "ACTIVE_DOWNLOAD_IDS";
    public static final String DOWNLOAD_PREFIX = "DOWNLOAD_";

    public static void CreateDownloadIntent(int id, String localPath, String remoteUri, boolean immediate)
    {
        Activity currentActivity = UnityPlayer.currentActivity;
        AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(currentActivity, OriumDownloadManager.class);
        intent.putExtra("id", id);
        intent.putExtra("localPath", localPath);
        intent.putExtra("remoteUri", remoteUri);

        if(immediate)
        {
            am.set(0, System.currentTimeMillis(),
                    PendingIntent.getBroadcast(currentActivity, id, intent, PendingIntent.FLAG_ONE_SHOT));
        }
        else
        {
            Calendar timeOffset = Calendar.getInstance();
            timeOffset.set(Calendar.HOUR_OF_DAY, 18);
            timeOffset.set(Calendar.MINUTE, 0);
            timeOffset.set(Calendar.SECOND, 0);

            long interval = TimeUnit.HOURS.toMillis(12);

            am.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP,
                    timeOffset.getTimeInMillis(),
                    interval,
                    PendingIntent.getBroadcast(currentActivity, id, intent, PendingIntent.FLAG_UPDATE_CURRENT));
        }
    }

    private static int QueryDownloadStatus(int id)
    {
        Activity currentActivity = UnityPlayer.currentActivity;
        DownloadManager dm = (DownloadManager) currentActivity.getSystemService(Context.DOWNLOAD_SERVICE);
        SharedPreferences sp = currentActivity.getSharedPreferences(OriumDownloadManager.class.getCanonicalName(),0);

        long downloadId = sp.getLong(DOWNLOAD_PREFIX + String.valueOf(id), Long.MAX_VALUE);
        if(downloadId == Long.MAX_VALUE)
        {
            return DownloadManager.STATUS_PENDING;
        }

        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);

        Cursor cursor = dm.query(query);

        if(!cursor.moveToFirst())
        {
            return DownloadManager.STATUS_PENDING;
        }

        return cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
    }

    public static boolean IsDownloadComplete(int id)
    {
        int status = QueryDownloadStatus(id);
        return status == DownloadManager.STATUS_FAILED || status == DownloadManager.STATUS_SUCCESSFUL;
    }

    public static boolean IsDownloadSuccessful(int id)
    {
        return QueryDownloadStatus(id) == DownloadManager.STATUS_SUCCESSFUL;
    }

    public static void CancelDownloadIntent(int id)
    {
        Activity currentActivity = UnityPlayer.currentActivity;
        AlarmManager am = (AlarmManager)currentActivity.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(currentActivity, OriumDownloadManager.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(currentActivity, id, intent, 0);
        am.cancel(pendingIntent);
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        try {
            int id = intent.getIntExtra("id",0);
            String localPath = intent.getStringExtra("localPath");
            String remoteUri = intent.getStringExtra("remoteUri");

            DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            SharedPreferences sp = context.getSharedPreferences(SHARED_PREFS_NAME, 0);

            File localFile = new File(Uri.parse(localPath).getPath());
            if(localFile.exists())
            {
                localFile.delete();
            }

            DownloadManager.Request request = new DownloadManager.Request(Uri.parse(remoteUri));
            request.setDestinationUri(Uri.parse(localPath));
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
            request.setTitle("Downloading Orium Set Bundle");
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE);

            long downloadId = dm.enqueue(request);

            Set<String> idSet = sp.getStringSet(ACTIVE_DOWNLOAD_IDS, new HashSet<String>());
            idSet.add(String.valueOf(id));
            SharedPreferences.Editor editor = sp.edit();
            editor.putStringSet(ACTIVE_DOWNLOAD_IDS, idSet);
            editor.putLong(DOWNLOAD_PREFIX + String.valueOf(id), downloadId);
            editor.commit();


//            Toast.makeText(context, "Download Enqueued", Toast.LENGTH_LONG).show();

//            int downloadStatus = QueryDownloadStatus(id);
//            while(downloadStatus != DownloadManager.STATUS_SUCCESSFUL &&
//                    downloadStatus != DownloadManager.STATUS_FAILED)
//            {
////                Toast.makeText(context,"Download Status: " + String.valueOf(downloadStatus), Toast.LENGTH_SHORT).show();
//                Thread.sleep(5000);
//            }

//            if(downloadStatus == DownloadManager.STATUS_SUCCESSFUL)
//            {
//                Notification.Builder builder = new Notification.Builder(context);
//
//                Intent notificationIntent = new Intent(context, com.unity3d.player.UnityPlayerActivity.class);
//                PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
//
//                builder.setContentIntent(contentIntent)
//                        .setWhen(System.currentTimeMillis())
//                        .setAutoCancel(true)
//                        .setContentTitle("Orium Set Bundle Downloaded")
//                        .setContentText("You're ready for doors-open...")
//                        .setSound(RingtoneManager.getDefaultUri(2))
//                        .setVibrate(new long[]{1000L, 1000L})
//                        .setSmallIcon(context.getResources().getIdentifier("notify_icon_small", "drawable", context.getPackageName()));;
//
//                NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//                nm.notify(1, builder.build());
//            }
        }
        catch (Exception e)
        {
            Toast.makeText(context, e.getLocalizedMessage(), Toast.LENGTH_LONG).show();
        }

//        try {
//            URL url = new URL(remoteUri);
//            URLConnection urlConnection = url.openConnection();



//            System.out.println("Date= "+new Date(urlConnection.getLastModified()));
//            System.out.println("Size= "+urlConnection.getContentLength());

//        } catch (MalformedURLException e1) {
//            e1.printStackTrace();  //Todo change body of catch statement.
//        } catch (IOException e1) {
//            e1.printStackTrace();  //Todo change body of catch statement.
//        }
    }
}
