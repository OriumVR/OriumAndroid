package com.Anima.Orium;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Richard on 14/04/2016.
 */
public class OriumDownloadCompleter extends BroadcastReceiver {

    private void MakeNotification(Context context)
    {
        Notification.Builder builder = new Notification.Builder(context);

        Intent notificationIntent = new Intent(context, com.google.unity.GoogleUnityActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

        builder.setContentIntent(contentIntent)
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentTitle("Orium Set Bundle Downloaded")
                .setContentText("You're ready for doors-open...")
                .setSound(RingtoneManager.getDefaultUri(2))
                .setVibrate(new long[]{1000L, 1000L})
                .setSmallIcon(context.getResources().getIdentifier("notify_icon_small", "drawable", context.getPackageName()));

        if(Build.VERSION.SDK_INT >= 21)
        {
            builder.setColor(Color.BLACK);
        }

        NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(1, builder.build());
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0L);
        SharedPreferences sp = context.getSharedPreferences(OriumDownloadManager.SHARED_PREFS_NAME, 0);

        Set<String> idSet = sp.getStringSet(OriumDownloadManager.ACTIVE_DOWNLOAD_IDS, new HashSet<String>());

        for (String idString: idSet) {
            long otherDownloadId = sp.getLong(OriumDownloadManager.DOWNLOAD_PREFIX + idString, Long.MAX_VALUE);
            if(downloadId == otherDownloadId)
            {
                idSet.remove(idString);
                sp.edit().putStringSet(OriumDownloadManager.ACTIVE_DOWNLOAD_IDS, idSet).commit();

                MakeNotification(context);

                return;
            }
        }
    }
}
